# Stage 1
FROM node:18-alpine3.18 AS node_builder

# Copy the code into the container. Note: copy to a dir instead of `.`, since Parcel cannot run in the root dir, see https://github.com/parcel-bundler/parcel/issues/6578
WORKDIR /build
COPY . .

# Build the static files
RUN corepack enable && corepack prepare pnpm@latest --activate
RUN pnpm install
RUN pnpm run build


# Stage 2: serve the files using nginx
FROM nginx:1.25.3-alpine3.18

COPY --from=node_builder /build/public /usr/share/nginx/html

COPY ./nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
