---
title : "FDS fieldlab overzicht"
description: "Frontend to visualize all available frontends for the FDS fieldlab."
date: 2023-10-27T09:14:40+02:00
draft: false
toc: true
---

## Running locally

Clone this repo. Note: static files (css/scss/js/img) are stored in the `static` dir, but built into the `public` dir.

For macOS:

```sh
export DOCKER_DEFAULT_PLATFORM=linux/amd64
```

Then,

```sh
docker build -t fds-fieldlab-overzicht .

docker run --rm --name fds-fieldlab-overzicht -p 8080:80 fds-fieldlab-overzicht
```

The website (without hot reload support) is then available at http://localhost:8080/

This starts a web server locally on the specified port.
